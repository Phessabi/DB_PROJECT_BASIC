
from django.conf.urls import url, include
from django.conf import settings
from django.conf.urls.static import static
from DB_Application.views import panel, user_name, main, login, message, msg_panel
from django.contrib import admin
from django.urls import path
from DB_Application.models import User

urlpatterns = [
    #path('/login', login , name='login'),
    path('', main, name = 'main'),
    #url(r'^/(?P<in_sid>[\w-]+)', main , name="main"),
    path('/panel', panel, name='panel'),
    path('/msg_panel', msg_panel, name='msg_panel'),
    path('/User', user_name, name='user_name'),
    path('/Mesg', message, name='message'),
]