from django.apps import AppConfig


class DbApplicationConfig(AppConfig):
    name = 'DB_Application'

