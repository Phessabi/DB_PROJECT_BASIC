# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models, connection, Error


class User(models.Model):
    number = models.IntegerField(unique=True)
    user_name = models.CharField(primary_key=True, max_length=30)
    picture = models.CharField(max_length=64, blank=True, null=True)
    status = models.CharField(max_length=30, blank=True, null=True)
    password = models.CharField(max_length=6)
    name = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'USER'


class Mesg(models.Model):
    user1 = models.ForeignKey(User, models.DO_NOTHING, db_column='user1')
    msg_id = models.IntegerField(primary_key=True)
    data_type = models.CharField(max_length=10, default= 'TEXT')
    data = models.CharField(max_length=512)
    sent_not = models.BooleanField(db_column='sent/not', default=False)  # Field renamed to remove unsuitable characters.
    seen_not = models.BooleanField(db_column='seen/not', default=False)  # Field renamed to remove unsuitable characters.
    edited_not = models.BooleanField(db_column='edited/not', default=False)  # Field renamed to remove unsuitable characters.
    secret_not = models.BooleanField(db_column='secret/not', default=False)  # Field renamed to remove unsuitable characters.
    reply_user = models.ForeignKey(User, models.DO_NOTHING, db_column='reply_user', blank=True, null=True, related_name='reply_user')
    forwarded = models.ForeignKey(User, models.DO_NOTHING, db_column='forwarded', blank=True, null=True, related_name='forwarded')
    reply_id = models.IntegerField(blank=True, null=True)
    class Meta:
        managed = False
        db_table = 'mesg'
        unique_together = (('user1', 'msg_id'),)

    def save(self, force_insert=False, force_update=False, using=False, update_fields=None):
        max = 0
        for i in Mesg.objects.all():
            if i.msg_id > max:
                max = i.msg_id

        with connection.cursor() as cursor:
            try:
                cursor.execute(
                    'INSERT INTO mesg (user1, data_type, data, msg_id)'
                        'VALUES (%s, %s, %s, %s);',[self.user1.user_name, self.data_type, self.data, max + 1]
                )
                self.msg_id = max + 1
                return True
            except Error:
                return False



class UserUserMsg(models.Model):
    user1 = models.ForeignKey(User, models.DO_NOTHING, db_column='user1', related_name="user1")
    user2 = models.ForeignKey(User, models.DO_NOTHING, db_column='user2', related_name="user2")
    msg_id = models.ForeignKey(Mesg, models.DO_NOTHING, db_column='msg_id', related_name="msg_id2", primary_key=True)

    class Meta:
        managed = False
        db_table = 'user_user_msg'
        unique_together = (('user1', 'msg_id', 'user2'),)
