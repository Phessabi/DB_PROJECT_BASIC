from django import forms
from DB_Application.models import User

class NameForm(forms.ModelForm):
    sid = forms.IntegerField(label='sid')
    name = forms.CharField(label='name')

    class Meta:
        model = User
        fields = ['sid', 'name']
