from django.contrib.auth.backends import ModelBackend

from DB_Application.models import Hello

class AuthenticationBackend(ModelBackend):
    def get_user(self, user_id):
        users = Hello.objects.raw('SELECT * FROM "hello" WHERE sid = %s', [user_id])
        for user in users:
            return user
        return None

    def authenticate(self, username=None, password=None, **kwargs):
        users = Hello.objects.raw('SELECT * FROM "User" WHERE sid = %s AND password = %s',
                                 [username, password])
        for user in users:
            user.is_authenticated = True
            user.is_anonymous = False
            return user
        return None
