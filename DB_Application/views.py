from django.shortcuts import render, redirect, get_object_or_404
from DB_Application.forms import NameForm
from django.http import HttpResponseRedirect
from DB_Application.models import User, Mesg, UserUserMsg
from django.contrib.auth import authenticate, login
from django.contrib import auth


def panel(request):
    array = []
    user_names = []
    for i in User.objects.raw('SELECT * FROM "USER"'):
        array.append((str(i.number), str(i.user_name), str(i.name), str(i.password)))
    #array.append("(number = " + str(i.number) + " , user_name = " + str(i.user_name) + ")")
    return render(request, 'panel/panel.html', context={'array': array},)

def msg_panel(request):
    array = []
    user_names = []
    for i in UserUserMsg.objects.raw('SELECT * FROM "user_user_msg"'):
        message_text = Mesg.objects.raw('SELECT * from "mesg" WHERE msg_id = %s', [i.msg_id.msg_id])
        for j in message_text:
            array.append((str(i.user1.user_name), str(i.user2.user_name), str(j.data)))
    #array.append("(number = " + str(i.number) + " , user_name = " + str(i.user_name) + ")")
    return render(request, 'msg_panel/msg_panel.html', context={'array': array},)


def user_name(request):
    array = []
    user_names = []
    #array.append("(number = " + str(i.number) + " , user_name = " + str(i.user_name) + ")")
    #return render(request, 'panel/panel.html', context={'array': array},)
    if request.method == 'POST':
        b = User(number=request.POST['number'], user_name=request.POST['your-user_name'], password=request.POST['password'], name=request.POST['your-name'])
        b.save()
        msg = "tuple (number = " + request.POST['number'] + " , user_name = '" + request.POST['your-user_name'] + "') added to USERS."
        for i in User.objects.raw('SELECT * FROM "USER"'):
            array.append((str(i.number), str(i.user_name), str(i.name), str(i.password)))
        return render(request, 'User/NewUser.html', {'message': msg, 'array': array})
    else:
        for i in User.objects.raw('SELECT * FROM "USER"'):
            array.append((str(i.number), str(i.user_name), str(i.name), str(i.password)))
        return render(request, 'User/NewUser.html', {'array': array})


def message(request):
    msg = ""
    array = []
    msarray = []
    if request.method == 'POST':
        u1 = None
        u2 = None
        for i in User.objects.raw('SELECT * FROM "USER" WHERE user_name = %s', [request.POST['user_name1']]):
            u1 = i.number
            break
        for i in User.objects.raw('SELECT * FROM "USER" WHERE user_name = %s', [request.POST['user_name2']]):
            u2 = i.number
            break
        if (u1 is None) or (u2 is None):
            msg = "atleast one user not found"
            #return render(request, 'Mesg/NewMesg.html', {'message': msg})
        else:
            user1 = User(number=u1, user_name=request.POST['user_name1'])
            user2 = User(number=u2, user_name=request.POST['user_name2'])
            b = Mesg(user1=user1, data=request.POST['message'], data_type='TEXT')
            b.save()
            b = UserUserMsg(user1=user1, user2=user2, msg_id=b)
            b.save()
            msg = "Message = " + request.POST['message'] + " , Sent From:'" + request.POST['user_name1'] + "' to:'" + request.POST['user_name2'] + "'"

        for i in UserUserMsg.objects.raw('SELECT * FROM "user_user_msg"'):
            message_text = Mesg.objects.raw('SELECT * from "mesg" WHERE msg_id = %s', [i.msg_id.msg_id])
            for j in message_text:
                msarray.append((str(i.user1.user_name), str(i.user2.user_name), str(j.data)))
        for i in User.objects.raw('SELECT * FROM "USER"'):
            array.append(str(i.user_name))
        return render(request, 'Mesg/NewMesg.html', {'message': msg, 'array': array, 'msarray': msarray})
    else:
        for i in UserUserMsg.objects.raw('SELECT * FROM "user_user_msg"'):
            message_text = Mesg.objects.raw('SELECT * from "mesg" WHERE msg_id = %s', [i.msg_id.msg_id])
            for j in message_text:
                msarray.append((str(i.user1.user_name), str(i.user2.user_name), str(j.data)))
        for i in User.objects.raw('SELECT * FROM "USER"'):
            array.append(str(i.user_name))
        return render(request, 'Mesg/NewMesg.html', {'array': array, 'msarray': msarray})


def main(request):
    number = -1
    in_number = 10
    user = ""
    """if in_number is not None:
            number = int(in_number)

    auth_user = User.objects.raw('SELECT * FROM "USER"')
    for i in auth_user:
        if i.number == number:
            user += "Welcome " + str(i.user_name) + "," + str(i.number)
            return render(request, 'main.html', {'user': user, 'number': in_number})"""
    return render(request, 'main.html', {'user': user, 'number': in_number})











"""def login(request, in_number = None):
    message = "Enter number"
    if request.method == 'POST':
        message = request.POST['number']
        entered = request.POST['number']
        auth_user = User.objects.raw('SELECT * FROM "User"')
        for i in auth_user:
            if str(i.number) == str(entered):
                return redirect('main', in_number = str(entered))
        message += ": Wrong credentials."
        return render(request, 'login/login.html', {'message': message})
    else:
        return render(request, 'login/login.html', {'message': message})
"""



"""form = user_nameForm(request.POST)
    message = "Enter number"
    if request.method == 'POST':
        message = request.POST['number']
        entered = request.POST['number']
        if form.is_valid():
            user = form.save(commit = False)
            user_id = form.cleaned_data['number']
            user_user_name = form.cleaned_data['user_name']
            user.setuser_name(user_user_name)
            user.setnumber(user_id)
            user.save()
            user = authenticate(number = user_id, user_name = user_user_name)
            if user is not None:
                login(request, user)
                #request.user.user_user_name
                return redirect('main', user_id)"""



